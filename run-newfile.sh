#!/usr/bin/env bash

Generate_tf.py --maxEvents="500" --ecmEnergy="13000" \
               --evgenJobOpts="MC15JobOpts-00-00-42_v3.tar.gz" \
               --firstEvent="1" \
               --jobConfig="MC15JobOptions/MC15.000000.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbbb_c10_M4000.py" \
               --outputEVNTFile="test.pool.root.1" \
               --randomSeed="4" \
               --runNumber="000000" \
               --AMITag="e3820"
