#!/usr/bin/env bash

set -eu

TAR=ATLMCPROD-8980

if [[ -d $TAR ]]; then
    echo "removing $TAR"
    rm -r $TAR
fi

mkdir $TAR

cp MC15JobOptions/MC15*.000000*.py $TAR

for f in outputs/**/log.txt
do
    LOGNAME=$(cut -d / -f 2 <<< $f)
    cp $f ${TAR}/${LOGNAME}.log
done

cp MC15JobOptions/MadGraphControl_RS_G_hh_bbbb.py $TAR

tar czf ${TAR}.tgz $TAR
