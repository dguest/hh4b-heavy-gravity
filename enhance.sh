#!/usr/bin/env bash

set -eu

if (( $# != 1 )); then
    echo "give a JO folder to enhance" >&2
    exit 1
fi

DIR=$1

echo "enhancing $DIR" >&2

if ! [[ -d $1 ]] ; then
    echo "$DIR needs to be a directory" >&2
    exit 1
fi

################################################################
# part 1: get the patched MadGraphControl_RS_G_hh_bbbb.py file
################################################################

(
    echo -n "getting updated job options control file..." >&2
    cd $DIR
    if [[ -f MadGraphControl_RS_G_hh_bbbb.py ]] ; then
        echo "already exists" >&2
    else
        wget -q https://gitlab.cern.ch/dguest/mc15joboptions/-/raw/regex-for-rs-g-parameters/common/MadGraph/MadGraphControl_RS_G_hh_bbbb.py
        echo "done"
    fi
)

###########################################################################
# Part 2: copy one of the existing JO files into new mass points
###########################################################################

# We start off with the BASE file below, but they all have the same
# contents: the control file has some magic in it to figure out what
# the parameters are from the file name. This way when we run
# Generate_tf.py the python code can read the parameters we want to
# use from the file name for this grid point.
#
# If this seems like a werid way to pass parameters Generate_tf, it's
# because it is. ATLAS is special. You are special too, but in a much
# better way <3 <3 <3
#
BASE=MC15.301491.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_hh_bbbb_c10_M600.py

# Key (first number) is the mass point, the DSID is the value. They
# are all dummy values for now.
declare -A DIDS=(
    [251]=000000
    [260]=000000
    [280]=000000
    [3500]=000000
    [4000]=000000
    [4500]=000000
    [5000]=000000
    [6000]=000000
)

# now make copies of the JO files
for MASS in ${!DIDS[@]}
do
    DSID=${DIDS[$MASS]-000000}
    NEW=$(echo $BASE | sed -e "s/M600/M${MASS}/g" -e "s/301491/${DSID}/")
    echo $NEW
    cp ${DIR}/${BASE} ${DIR%/}/${NEW}
done
