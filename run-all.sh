#!/usr/bin/env bash

set -eu

OUTDIR=outputs

mkdir -p ${OUTDIR}
cd $OUTDIR

for JO in ../MC15JobOptions/MC15*.000000.*.py
do
    echo "running $JO"
    JO_FULL=$(readlink -e $JO)
    NAME=${JO##*/}
    RUN=$(echo $NAME | cut -d . -f 2)
    SHORTNAME=${NAME%.py}
    if [[ -d $SHORTNAME ]]; then
        echo "$SHORTNAME exists, skipping"
        continue
    fi
    mkdir -p $SHORTNAME
    (
        cd $SHORTNAME
        Generate_tf.py --maxEvents="50" --ecmEnergy="13000" \
                       --evgenJobOpts="MC15JobOpts-00-00-42_v3.tar.gz" \
                       --firstEvent="1" \
                       --jobConfig=$JO_FULL \
                       --outputEVNTFile="output.pool.root.1" \
                       --randomSeed="4" \
                       --runNumber=$RUN \
                       --AMITag="e3820" | tee log.txt
    )
done
