Graviton to HH to bbbb grid extension
=====================================

This is a collection of scripts to test the signal grid extension. I
don't really know what I'm doing here, so if you're an MC generation
expert you could probably clean it up.

Quickstart
----------

1. Clone this project

2. make a `run` directory _next to_ it (not in it)

4. go into `run` and run `../hh4b-heavy-gravity/setup.sh`. This should
   set up the release.

5. Run `../hh4-heavy-gravity/run.sh` to run a test on an existing sample.

This should create a lot of output (which doesn't end in an error) and
fill your `run` directory with lots of files.


Adding more signal points
-------------------------

The code uses `MadGraphControl_RS_G_hh_bbbb.py` to steer a lot of options. This needs to be updated to make it accommodate higher mass points. Fortunately I already have an updated file to do this, which is in [a closed merge request][1].

We need to add this file to `MC15JobOptions/` so that it gets picked
up instead of the default one. Then we have to make renamed copies of
the JopOptions files themselves, which specify the signal points. Both
of these are done in the same script, called `enhance.sh`.

Run `../hh4b-heavy-gravity/enhance.sh MC15JobOptions/` to make the new
JO files.

Now test them with `../hh4b-heavy-gravity/run-newfile.sh`


Generating the tarball for [ATLMCPROD-8980](https://its.cern.ch/jira/browse/ATLMCPROD-8980)
-------------------------------------------------------------------------------------------

You can run `run-all.sh` from whatever directory you ran the `enhance.sh`
script from. This will generate an `outputs` directory and run each set of
job otions that `enhance.sh` created.

A second script called `harvest.sh` will package the log files and new job
options into a `.tgz` file.


To Do
-----

 - [x] Figure out what the DSIDS are supposed to be and assign them in
       the enhance script
 - [x] Get hold of the MC contact and get these scripts updated

[1]: https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/merge_requests/36
